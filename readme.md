# Türkiye İller ve İlçeler API

### Node.js öğrenirken yaptığım Türkiye'nin il ve ilçe bilgilerini bulabileceğiniz küçük bir API.

## `/api/iller`

### Tüm illeri ve bilgilerini döndürür.

## `/api/iller/:il`

### İsmi girilen ilin tüm bilgilerini döndürür. (küçük harflerle)

## `api/ilceler/:il`

### İsmi girilen ilin ilçelerini döndürür. (küçük harflerle)

# Kullanılan teknolojiler

- NodeJS
- MongoDB
- Express

## Çalıştırmak için

1. `git clone https://github.com/umutbozdag/tr-il-ilceler-api.git`
2. `npm start`

#### Kullanılan dataset

[Link](https://github.com/enisbt/turkey-cities)

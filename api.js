const express = require("express");
const PORT = process.env.PORT || 3000;
const MongoClient = require("mongodb").MongoClient;
const app = express();
require("dotenv").config();

const uri = `mongodb+srv://dbUser:deneme@cluster0-cxuby.mongodb.net/test?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

app.get("/", (req, res) => {
  res.send("Hello world");
});

app.get("/api/iller", (req, res) => {
  client.connect((err, db) => {
    const dbo = db.db("TURKIYEAPI");
    const collection = dbo.collection("iller_ilceler");

    collection.find({}).toArray((err, result) => {
      if (err) throw err;

      res.status(200).send(result);
    });
  });
});

app.get("/api/iller/:il", (req, res) => {
  client.connect((err, db) => {
    const dbo = db.db("TURKIYEAPI");
    const collection = dbo.collection("iller_ilceler");

    collection.find({ name: req.params.il }).toArray((err, result) => {
      if (err) throw err;

      res.status(200).send(result);
    });
  });
});

app.get("/api/ilceler/:il", (req, res) => {
  client.connect((err, db) => {
    const dbo = db.db("TURKIYEAPI");
    const collection = dbo.collection("iller_ilceler");

    collection
      .find({ name: req.params.il }, { projection: { counties: 1 } })
      .toArray((err, result) => {
        if (err) throw err;

        res.status(200).send(result);
      });
  });
});

app.listen(PORT);
